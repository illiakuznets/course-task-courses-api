export enum EducationLevel {
  BACHELOR = 'Bachelor',
  MASTER = 'Master',
}

export enum CourseSeason {
  AUTUMN = 'Autumn',
  SPRING = 'Spring',
  SUMMER = 'Summer',
}
